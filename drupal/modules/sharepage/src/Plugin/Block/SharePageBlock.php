<?php

namespace Drupal\sharepage\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Component\Utility\UrlHelper;

/**
 * Provides a 'Share Page' Block.
 *
 * @Block(
 *   id = "sharepage_block",
 *   admin_label = @Translation("Share Page"),
 *   category = @Translation("Share Page"),
 * )
 */
class SharePageBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $uri = \Drupal::request()->getUri();
    $uri_encode = UrlHelper::encodePath($uri);
    
    return [
      '#theme' => 'sharepage_block',
      '#uri' => $uri,
      '#uri_encode' => $uri_encode,
      '#attached' => [
        'library' => [
          'sharepage/sharepage',
        ],
      ],
    ];
  }

}

